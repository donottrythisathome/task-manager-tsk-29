package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove user by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID");
        @NotNull final String userId = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().removeOneById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
    }

    @Override
    @NotNull
    public String name() {
        return "user-remove-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
