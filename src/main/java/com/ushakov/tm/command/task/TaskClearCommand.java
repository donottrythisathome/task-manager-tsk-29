package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Clear task list.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskService().clear(userId);
    }

    @Override
    @NotNull
    public String name() {
        return "task-clear";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
