package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK INDEX");
        @Nullable final Integer taskIndex = TerminalUtil.nextNumber();
        @Nullable final Task task = serviceLocator.getTaskService().removeOneByIndex(userId, taskIndex - 1);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    @NotNull
    public String name() {
        return "remove-task-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
