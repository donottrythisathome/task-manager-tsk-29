package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK INDEX");
        final int taskIndex = TerminalUtil.nextNumber();
        @NotNull final Task task = serviceLocator.getTaskService().findOneByIndex(userId, taskIndex - 1);
        System.out.println("ENTER NAME");
        @NotNull final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String taskDescription = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateByIndex(userId, taskIndex - 1, taskName, taskDescription);
    }

    @Override
    @NotNull
    public String name() {
        return "update-task-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
